package main

import (
	"exam-gits/soal1"
	"exam-gits/soal2"
	"fmt"
)

// Butuh menu untuk ke soal 1 dan soal 2

func main() {

	var (
		ulang bool = true
		input int
		err   error
	)

	for ulang {
		err = nil
		input = 0
		fmt.Println("==========")
		fmt.Println("Gits.ID Golang Native Exam")
		fmt.Println("by Kaenova Mahendra Auditama and M Alif Naufal Yasin")
		fmt.Println("Paket B")
		fmt.Println("==========")
		fmt.Println("Menu:")
		fmt.Println("1. Nomor 1")
		fmt.Println("2. Nomor 2")
		fmt.Println("3. Keluar")
		fmt.Print("Pilih menu : ")
		_, err = fmt.Scanln(&input)
		if err != nil {
			fmt.Println("Masukkan tidak valid")
		}

		switch input {
		case 1:
			soal1.Menu()
		case 2:
			soal2.Menu()
		case 3:
			ulang = false
		default:
		}
	}
}
