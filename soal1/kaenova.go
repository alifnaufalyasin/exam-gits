package soal1

import (
	"errors"
	"fmt"
)

/*
	Note: Kaenova
	Kalau misalkan mau ganti-ganti data mending bentuknya Fungsi aja, tapi
	kalau hanya ingin menampilkan, lebih baik menggunakan prosedur
*/

func Menu() {
	var (
		ulang bool = true
		input int
		err   error
	)

	var dataGlobal []DataCustomer

	for ulang {
		err = nil
		input = 0
		fmt.Println("=======================")
		fmt.Println("Soal 1")
		fmt.Println("Pengolahan Data Customer")
		fmt.Println("=======================")
		fmt.Println("Menu:")
		fmt.Println("1. Input Data")
		fmt.Println("2. Hapus Data by ID")
		fmt.Println("3. Tampilkan Keseluruhan Data dan Jumlah Data")
		fmt.Println("4. Tampilkan Rata-Rata Jumlah Jam Penggunaan Customer")
		fmt.Println("5. Tampilkan 3 Customer dengan Jam Penggunaan Paling Sedikit")
		fmt.Println("6. Tampilakan Customer dengan Penggunaan Kurang Dari Rata-Rata")
		fmt.Println("7. Keluar")
		fmt.Print("Pilih menu: ")

		_, err = fmt.Scanln(&input)
		if err != nil {
			fmt.Println("Masukkan tidak valid")
		}
		fmt.Println("=======================")

		switch input {
		case 1:
			dataGlobal, err = AddData(dataGlobal)
		case 2:
			dataGlobal, err = HapusData(dataGlobal)
		case 3:
			TampilkanSeluruhData(dataGlobal)
		case 4:
			rataRata := RataRataCustomer(dataGlobal)
			fmt.Printf("Rata-Rata Jam Penggunaan Seluruh Customer : %.2f\n", rataRata)
		case 5:
			TampilJamSedikit(dataGlobal, 3)
		case 6:
			TampilkanDataDibawahRataRata(dataGlobal)
		case 7:
			var keluar string = ""
			fmt.Println("Apakah anda yakin ingin keluar?")
			fmt.Println("Dengan keluar, akan menghapus data yang sudah diinput")
			fmt.Print("(Y/N) ? ")
			_, err = fmt.Scanln(&keluar)
			if err != nil {
				fmt.Println("Masukkan tidak valid")
			}
			if keluar == "Y" {
				ulang = false
			}
		default:
		}
		if err != nil {
			fmt.Println(err.Error())
		}
	}
}

func HapusData(data []DataCustomer) ([]DataCustomer, error) {

	var (
		id  string
		err error
	)
	fmt.Print("Masukkan id yang diinginkan : ")
	_, err = fmt.Scanln(&id)
	if err != nil {
		return nil, errors.New("masukkan tidak valid")
	}

	/*
		1. Search ID untuk mendapatkan Index
		2. Hapus berdasarkan Index
	*/

	var (
		index int = -1
	)

	// Search index
	for i, currData := range data {
		if currData.Id == id {
			index = i
			break
		}
	}
	if index == -1 {
		return data, errors.New("tidak ditemukan id yang sesuai")
	}

	// Deleting data by index
	kiri := data[:index]
	kanan := data[index+1:]
	data = append(kiri, kanan...)
	fmt.Println("Berhasil menghapus data")
	return data, nil

}

func RataRataCustomer(data []DataCustomer) float32 {
	var jumlah int = 0

	if len(data) == 0 {
		fmt.Println("Data masih kosong")
		return 0
	}

	// Hitung total jam keseluruhan customer
	for _, dataCurr := range data {
		jumlah = jumlah + dataCurr.JamPenggunaan
	}
	// Cari banyak data customer
	jumlahCustomer := len(data)
	// Hitung rata-ratanya
	rataRata := float32(jumlah) / float32(jumlahCustomer)

	return rataRata
}

func TampilkanSeluruhData(data []DataCustomer) {
	DataCustomerPrinter(data)
	fmt.Println("Total Data : ", len(data))
}

func DataCustomerPrinter(data []DataCustomer) {
	/*
		Digunakan untuk print DataCustomer dalam bentuk tabluar
	*/

	// Jika Data Kosong
	if len(data) == 0 {
		fmt.Println("Data Kosong")
		return
	}

	fmt.Printf("%-10s %-18s %-15s %-10s", "ID", "Nama", "Jam Penggunaan", "Biaya (Rp)\n")
	for _, dataCurr := range data {
		fmt.Printf("%-10s %-18s %-15d %-10d\n", dataCurr.Id, dataCurr.Nama, dataCurr.JamPenggunaan, dataCurr.Biaya)
	}
}
