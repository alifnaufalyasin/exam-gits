package soal1

import (
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
	"sort"
)

func RandToken(n int) (string, error) {
	bytes := make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	return hex.EncodeToString(bytes), nil
}

func CekId(data []DataCustomer, id string) bool {
	var (
		index int = -1
	)

	// Search index
	for i, currData := range data {
		if currData.Id == id {
			index = i
			break
		}
	}

	return index != -1
}

func CheckNama(nama string) error {
	if nama == "" {
		return errors.New("nama tidak boleh kosong")
	} else {
		return nil
	}
}

func HitungBiaya(jam int) int {
	return jam * 60 * 1000
}

func AddData(data []DataCustomer) ([]DataCustomer, error) {
	var nama string
	var jam, biaya int
	var err error

	fmt.Print("Masukkan Nama : ")
	fmt.Scanln(&nama)
	err = CheckNama(nama)

	if err != nil {
		return data, err
	}

	fmt.Print("Masukkan Jam Penggunaan : ")
	fmt.Scanln(&jam)
	biaya = HitungBiaya(jam)

	id, _ := RandToken(2)
	ok := CekId(data, id)
	for ok {
		id, _ = RandToken(2)
		ok = CekId(data, id)
	}

	data = append(data, DataCustomer{
		Id:            id,
		Nama:          nama,
		JamPenggunaan: jam,
		Biaya:         biaya,
	})

	return data, nil
}

func TampilJamSedikit(data []DataCustomer, banyaknya int) {

	sort.Slice(data, func(i, j int) bool {
		return data[i].JamPenggunaan < data[j].JamPenggunaan
	})

	if len(data) > banyaknya {
		TampilkanSeluruhData(data[:banyaknya])
	} else {
		TampilkanSeluruhData(data)
	}

}

func TampilkanDataDibawahRataRata(data []DataCustomer) {
	var tempData []DataCustomer
	var jamRataRata = RataRataCustomer(data)
	for _, val := range data {
		if float32(val.JamPenggunaan) < jamRataRata {
			tempData = append(tempData, val)
		}
	}

	TampilkanSeluruhData(tempData)
}
