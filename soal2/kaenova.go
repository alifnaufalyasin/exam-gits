package soal2

import (
	"fmt"
	"math"
)

func Menu() {
	var (
		ulang bool = true
		input int
		err   error
	)

	for ulang {
		err = nil
		input = 0
		fmt.Println("=======================")
		fmt.Println("Soal 2")
		fmt.Println("Rekursif")
		fmt.Println("=======================")
		fmt.Println("Menu:")
		fmt.Println("1. Sum = 1 + 20 + 300 + 4000 + 50000 + ....")
		fmt.Println("2. Sum = 1/2x + 2/4x + 3/6x + 4/8x + ....")
		fmt.Println("3. Menghitung huruf non kapital")
		fmt.Println("4. Keluar")
		fmt.Print("Pilih menu: ")

		_, err = fmt.Scanln(&input)
		if err != nil {
			fmt.Println("Masukkan tidak valid")
		}
		fmt.Println("=======================")

		switch input {
		case 1:
			Nomor1()
		case 2:
			Nomor2()
		case 3:
			Nomor3()
		case 4:
			ulang = false
		default:
		}

	}

}

func Nomor1() {

	var Rekursif func(max_iter, current int) float64

	Rekursif = func(max_iter, current int) float64 {
		current_real := current + 1
		now := float64(current_real) * math.Pow(10, float64(current))

		if current == max_iter-1 {
			fmt.Printf("%.0f = ", now)
			return now
		} else {
			fmt.Printf("%.0f + ", now)
			return now + Rekursif(max_iter, current+1)
		}
	}

	var (
		max int
		err error
	)
	err = nil

	fmt.Print("Masukkan jumlah unsur : ")
	_, err = fmt.Scanln(&max)
	if err != nil {
		fmt.Println("Masukkan tidak valid")
	} else {
		fmt.Print("Sum = ")
		fmt.Printf("%.0f\n", Rekursif(max, 0))
	}
}

func Nomor3() {

	var Rekursif func(kata string) uint
	Rekursif = func(kata string) uint {
		var hasil uint
		current := byte(kata[0])
		if !((current >= byte('A')) && (current <= byte('Z'))) {
			hasil = 1
		} else {
			hasil = 0
		}
		if len(kata) == 1 {
			return hasil
		} else {
			kata_baru := kata[1:]
			return hasil + Rekursif(kata_baru)
		}
	}

	var kata string
	fmt.Print("Masukkan kata: ")
	fmt.Scanln(&kata)
	fmt.Println("Jumlah huruf non kapital :", Rekursif(kata))
}
