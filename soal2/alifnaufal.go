package soal2

import (
	"fmt"
)

func Nomor2() {
	var Rekursif func(n, max int) float64

	Rekursif = func(n, max int) float64 {
		b := n * 2
		bawah := float64(float64(n) * 2)
		if n < max {
			fmt.Printf("%d/%dx + ", n, b)
			return float64(float64(n)/bawah) + Rekursif(n+1, max)
		} else {
			fmt.Printf("%d/%dx", n, b)
			return float64(float64(n) / bawah)
		}
	}

	var (
		max int
		err error
	)
	err = nil

	fmt.Print("Masukkan jumlah unsur : ")
	_, err = fmt.Scanln(&max)
	if err != nil {
		fmt.Println("Masukkan tidak valid")
	} else {
		fmt.Print("Sum = ")
		fmt.Printf(" = %.2fx\n", Rekursif(1, max))
	}
}
